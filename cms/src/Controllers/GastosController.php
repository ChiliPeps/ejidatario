<?php

namespace Nhitrort90\CMS\Controllers;
use Carbon\Carbon;
use Nhitrort90\CMS\Modules\Gastos\GastosRepo;
use Nhitrort90\CMS\Requests\CreateGasto;
use Nhitrort90\CMS\Requests\UpdateGasto;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GastosController extends Controller
{

    public function index()
    {
        return view('CMS::gastos.index');
    }

    public function create()
    {

    }


    public function store(CreateGasto $request)
    {

    }

    public function edit($id)
    {

    }


    public function update($id, UpdateGasto $request)
    {

    }

    public function destroy($id)
    {

    }
}
