<?php

namespace Nhitrort90\CMS\Controllers;
use Carbon\Carbon;
use Nhitrort90\CMS\Modules\Clientes\ClientesRepo;
use Nhitrort90\CMS\Requests\CreateCliente;
use Nhitrort90\CMS\Requests\UpdateCliente;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClientesController extends Controller
{

    public function index()
    {
        return view('CMS::clientes.index');
    }

    public function create()
    {

    }


    public function store(CreateCliente $request)
    {

    }

    public function edit($id)
    {

    }


    public function update($id, UpdateCliente $request)
    {

    }

    public function destroy($id)
    {

    }
}
