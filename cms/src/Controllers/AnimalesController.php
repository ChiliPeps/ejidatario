<?php

namespace Nhitrort90\CMS\Controllers;
use Carbon\Carbon;
use Nhitrort90\CMS\Modules\Animales\AnimalesRepo;
use Nhitrort90\CMS\Requests\CreateAnimal;
use Nhitrort90\CMS\Requests\UpdateAnimal;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AnimalesController extends Controller
{

    public function index()
    {
        return view('CMS::animales.index');
    }

    public function create()
    {

    }


    public function store(CreateAnimal $request)
    {

    }

    public function edit($id)
    {

    }


    public function update($id, UpdateAnimal $request)
    {

    }

    public function destroy($id)
    {

    }
}
