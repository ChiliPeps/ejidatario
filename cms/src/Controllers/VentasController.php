<?php

namespace Nhitrort90\CMS\Controllers;
use Carbon\Carbon;
use Nhitrort90\CMS\Modules\Ventas\VentasRepo;
use Nhitrort90\CMS\Requests\CreateVenta;
use Nhitrort90\CMS\Requests\UpdateVenta;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VentasController extends Controller
{

    public function index()
    {
        return view('CMS::ventas.index');
    }

    public function create()
    {

    }


    public function store(CreateVenta $request)
    {

    }

    public function edit($id)
    {

    }


    public function update($id, UpdateVenta $request)
    {

    }

    public function destroy($id)
    {

    }
}
