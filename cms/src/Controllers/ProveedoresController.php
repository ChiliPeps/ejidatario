<?php

namespace Nhitrort90\CMS\Controllers;
use Carbon\Carbon;
use Nhitrort90\CMS\Modules\Proveedores\ProveedoresRepo;
use Nhitrort90\CMS\Requests\CreateProveedor;
use Nhitrort90\CMS\Requests\UpdateProveedor;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProveedoresController extends Controller
{

    public function index()
    {
        return view('CMS::proveedores.index');
    }

    public function create()
    {

    }


    public function store(CreateProveedor $request)
    {

    }

    public function edit($id)
    {

    }


    public function update($id, UpdateProveedor $request)
    {

    }

    public function destroy($id)
    {

    }
}
