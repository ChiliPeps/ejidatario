<?php

namespace Nhitrort90\CMS\Controllers;
use Carbon\Carbon;
use Nhitrort90\CMS\Modules\Lotes\LotesRepo;
use Nhitrort90\CMS\Requests\CreateLote;
use Nhitrort90\CMS\Requests\UpdateLote;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LotesController extends Controller
{

    public function index()
    {
        return view('CMS::lotes.index');
    }

    public function create()
    {

    }


    public function store(CreateLote $request)
    {

    }

    public function edit($id)
    {

    }


    public function update($id, UpdateLote $request)
    {

    }

    public function destroy($id)
    {

    }
}
