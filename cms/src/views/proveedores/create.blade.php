@extends('CMS::master')

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-file"></i> @lang('CMS::proveedores.test_proveedores')
  </h1>
</section>

<section class="content">
    @include('CMS::proveedores.partials.inputs')
</section>
@stop

@section('scripts')
    @include('CMS::proveedores.partials.scripts')
@stop