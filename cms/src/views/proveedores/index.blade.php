@extends('CMS::master')

@section('content')
<style>
    .table thead{
        color:white;
    }
    .table td {
       text-align: center;    
    }
    .table th {
        text-align: center; 
        background: #2980b9;
    }
    tr:nth-child(even) {
      background: #e9e9e9;
    }
    /*loading*/
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
    .iconoload{
        position:fixed;
        top:50%;
    }
</style>
    <section class="content-header">
        <h1>
            <i class="fa fa-tty"></i> Proveedores
        </h1>
    </section>

     <section id ="app" v-cloak>
        <div class="content" v-show="panelIndex">
            <div class="box box-primary" >
                <div class="box-header with-border" >
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        <button class ="btn btn-block btn-primary" v-on:click=""> Registrar Proveedor </button>
                    </div>


                        {{-- <filtersearch :selected="seleccion" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}

                   
                </div>

                <div class="box-body">

                    <div class="text-center cargando" v-show="loading">
                        <i class="fa fa-spinner fa-spin fa-5x iconoload"></i>
                    </div>
           
                    <div class="table-responsive" >
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>

                                    <th>Razon Social</th>
                                    <th>RFC</th>
                                    <th>Direccion</th>
                                    <th>Telefono</th>
                                    <th>Correo</th>                               
                                </tr>
                            </thead>
                        <tbody>
                        <tr>{{-- <tr  v-for="s in dataJson"> --}}
                                <td>Rancho Tijuana</td>
                                <td>RANC119923L00</td>
                                <td>Calle 1 / Calle 2 COL. Cerro colorado</td>
                                <td>664123123</td>
                                <td>Rancho_tijuana@hotmail.com</td>
                                
                            </tr>
                        </tbody>
                        </table>

                    </div>

                    {{-- <pagination @setpage="GetTuchmanCitas" :param="pagination"></pagination> --}}

                   
                </div>
            </div>
           
        </div>
{{--          @include('CMS::citacuraes.create') --}}

        
    </section>

@endsection

@section('scripts')
    @include('CMS::proveedores.partials.scripts')
@stop