{{-- Mixins --}}
@include('CMS::partials.vueNotifications')

{{-- Components --}}
@include('inc.vuecomponents.vueModal')
@include('CMS::partials.vuePagination')
@include('inc.vuecomponents.vueSelect2_multi_tags')
@include('inc.vuecomponents.vueCalendar')
{{-- @include('inc.vuecomponents.vueSelect2') --}}
@include('CMS::partials.vueHelperFunctions')
@include('CMS::partials.vueFilterSearch')

<script>
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

    var app = new Vue ({

        mounted: function () {
        },

        el: '#app',

        mixins: [helperFunctions, notifications],

        data: {
            busqueda: '', //textbox de busqueda
            seleccion: 'fullName',


            tipos:[
                {text: 'Nombre completo', value:'fullName'},
                {text: 'Apellido Paterno', value:'Apellidopaterno'},
                {text: 'Apellido Materno', value:'Apellidomaterno'},
                {text: 'Especialidad', value:'nombre'},
                {text: 'Fecha', value:'fecha'},
                // {text: 'Fecha', value:'fecha'},
                ],
           	panelIndex:true,
           	// loading:true,
            pagination:null,
            dataJson:null,
            public_url: "{{ URL::to('/') }}/",
           

        },

        computed: {

        },
        methods:{

            GetTuchmanCitas: function(url){
                this.loading = true;
                var resource = this.$resource(url);
                var data = { busqueda: this.busqueda, tipo: this.seleccion }
                resource.get(data).then(function (response) {
                    this.pagination = response.data.cit;
                    this.dataJson = response.data.cit.data;
                    this.infoEsp = response.data.esp;
                    this.loading = false;
                });
            },

            borrar:function(id){
            swal({ title: "¿Estas seguro?",   
                text: "Esta a punto de borrar una cita!", 
                type: "warning",   showCancelButton: true, 
                confirmButtonColor: "#DD6B55", 
                confirmButtonText: "Si, borralo!", 
                closeOnConfirm: true }).then(function(){
                    this.delete({ id: id }, "");
                }.bind(this))
            
            },
             
             //Update Filter Data
            updateFilters: function (data) {
                this.busqueda = data.busqueda;
                this.seleccion = data.tipo;
                this.GetTuchmanCitas(this.dataRoute);
            },

             updateFiltersCliente: function (data) {
                this.busquedaCliente = data.busqueda;
                this.seleccion = data.tipo;
                this.getUsers(this.usersRoute);
            },

            regresar: function (){
                // debugger;
               
                // this.botonGuardar=true;
                // this.botonActualizar=false;
            },
            panelCrear: function(){
                // this.panelIndex = false;
                // this.createPanel = true;
            },

                    
            modal:function(idRecord)
            {   
                // this.showModal=true

                // var id = this.findIndexByKeyValue(this.dataJson,'CITAID', idRecord);
                // this.nota  = this.dataJson[id]['NOTES']
                
            },

           
            generateFormData: function (action) {
                // var form = new FormData();

                // if(action == "update") {
                //     form.append('id', this.citanew.CITAID);
                // }
                
                // form.append('idespecialidad', this.citanew.idespecialidad);                
                // form.append('newpacid', this.citanew.newpacid);
                // form.append('fecha', this.citanew.fecha);
                // form.append('start_time', this.citanew.hora);
                // form.append('mensaje', this.citanew.mensaje);
                // form.append('estatus' ,this.citanew.estatus);    

                return form;
            },

            guardarCita:function()
            {   
                var routeSave = "";

                this.save(this.generateFormData("insert"), routeSave, "insert");

            },
            actualizar:function(idRecord)
            {   
                                
            },
            actualizarCita:function()
            {
               
                // this.save(this.generateFormData("update"), routeUpdate, "update");
            },

            //VUE HTTP RESOURCES
            save: function (data, route, action) {
                this.loading = true;
                var resource = this.$resource(route)
                resource.save(data).then(function (response) {                    
                    
                    // this.saveInAction = false;
                    if(action == "update") {
                        this.notification('fa fa-check-circle', 'OK!', "Cita actualizada correctamente.", 'topCenter');
                    } else {
                        this.notification('fa fa-check-circle', 'OK!', "Cita ingresada correctamente.", 'topCenter');
                    }
                    
                    this.regresar();
                    this.loading = false;
                    this.GetTuchmanCitas(this.dataRoute);
                }, function (response) {
                    this.loading = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
                    // this.errorsBag = response.data;
                })
            },

            delete: function (data, route) {
               
                var resource = this.$resource(route)
                resource.delete(data).then(function (response) {
                    this.notification('fa fa-check-circle', 'OK!', "Entrada borrada correctamente.", 'topCenter');
                    this.GetTuchmanCitas(this.dataRoute);
                }, function (response) {
                    this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
                })
            },
           
        }
    });

</script>