@extends('CMS::master')

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-file"></i> @lang('CMS::ventas.test_ventas')
  </h1>
</section>
@stop

@section('scripts')
    @include('CMS::ventas.partials.scripts')
@stop