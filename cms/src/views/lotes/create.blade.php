@extends('CMS::master')

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-file"></i> @lang('CMS::lotes.test_lotes')
  </h1>
</section>

<section class="content">
    @include('CMS::lotes.partials.inputs')
</section>
@stop

@section('scripts')
    @include('CMS::lotes.partials.scripts')
@stop