@extends('CMS::master')

@section('content')
<style>
    .table thead{
        color:white;
    }
    .table td {
       text-align: center;    
    }
    .table th {
        text-align: center; 
        background: #2980b9;
    }
    tr:nth-child(even) {
      background: #e9e9e9;
    }
    /*loading*/
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
    .iconoload{
        position:fixed;
        top:50%;
    }
</style>
    <section class="content-header">
        <h1>
            <i class="fa fa-archive"></i> Lotes Comprados
        </h1>
    </section>

     <section id ="app" v-cloak>
        <div class="content" v-show="panelIndex">
            <div class="box box-primary" >
                <div class="box-header with-border" >
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        <button class ="btn btn-block btn-primary" v-on:click=""> Registrar Lote </button>
                    </div>


                        {{-- <filtersearch :selected="seleccion" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}

                   
                </div>
                <div class="box-body">

                    <div class="text-center cargando" v-show="loading">
                        <i class="fa fa-spinner fa-spin fa-5x iconoload"></i>
                    </div>

                    <div class="table-responsive" >
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>

                                    <th>nombre</th>
                                    <th>Cantidad</th>
                                    <th>Precio de compra</th>
                                    <th>Peso/Total</th>
                                    <th>Peso/Promedio</th>    
                                    <th>Fecha</th>
                                    <th>Adquirido De:</th>   
                                    <th>Gastos/Totales</th>  
                                    <th>Agregar gastos</th>
                                    <th></th>
                                </tr>
                            </thead>
                        <tbody>
                        <tr>{{-- <tr  v-for="s in dataJson"> --}}
                                <td>Lote1</td>
                                <td>30</td>
                                <td>$300,000.00</td>
                                <td>300000 KG</td>
                                <td>100 KG</td>
                                <td>8-MAY-2010</td>
                                <td>
                                    Rancho Del Valle
                                </td>
                                <td>
                                    $18,975.00
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success"><i class="fa fa-eye"></i></button>
                                    <button type="button" class="btn btn-warning"><i class="fa fa-plus"></i></button>
                                </td>
                                <td>
                                    <button type="button" class="btn-block btn-danger">Eliminar</i></button>
                                    <button type="button" class="btn-block btn-warning">Editar</i></button>
                                </td>
                                

                                
                            </tr>
                        </tbody>
                        </table>

                    </div>

                                    {{-- <pagination @setpage="GetTuchmanCitas" :param="pagination"></pagination> --}}

                                   
                </div>


                
            </div>
           
        </div>
{{--          @include('CMS::citacuraes.create') --}}

        
    </section>

@endsection

@section('scripts')
    @include('CMS::proveedores.partials.scripts')
@stop