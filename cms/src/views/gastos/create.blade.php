@extends('CMS::master')

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-file"></i> @lang('CMS::gastos.test_gastos')
  </h1>
</section>

<section class="content">
    @include('CMS::gastos.partials.inputs')
</section>
@stop

@section('scripts')
    @include('CMS::gastos.partials.scripts')
@stop