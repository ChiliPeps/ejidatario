@extends('CMS::master')

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-file"></i> @lang('CMS::clientes.test_clientes')
  </h1>
</section>
@stop

@section('scripts')
    @include('CMS::clientes.partials.scripts')
@stop