@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-file"></i> @lang('CMS::clientes.test_clientes')
        </h1>
    </section>
@endsection