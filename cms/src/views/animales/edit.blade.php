@extends('CMS::master')

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-file"></i> @lang('CMS::animales.test_animales')
  </h1>
</section>
@stop

@section('scripts')
    @include('CMS::animales.partials.scripts')
@stop