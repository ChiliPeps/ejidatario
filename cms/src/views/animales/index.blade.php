@extends('CMS::master')

@section('content')
<style>
    .table thead{
        color:white;
    }
    .table td {
       text-align: center;    
    }
    .table th {
        text-align: center; 
        background: #2980b9;
    }
    tr:nth-child(even) {
      background: #e9e9e9;
    }
    /*loading*/
    .cargando{
        width: 100%;
        height:100%;
        position:absolute;
        background:rgba(255,255,255,0.7);
        left:0;
        z-index: 1;
    }
    .iconoload{
        position:fixed;
        top:50%;
    }
</style>
    <section class="content-header">
        <h1>
            <i class="fa fa-asterisk"></i> Animales
        </h1>
    </section>

     <section id ="app" v-cloak>
        <div class="content" v-show="panelIndex">
            <div class="box box-primary" >
                <div class="box-header with-border" >
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        {{-- <button class ="btn btn-block btn-primary" v-on:click=""> Registrar Animal </button> --}}
                    </div>


                        {{-- <filtersearch :selected="seleccion" :options="tipos" @updatefilters="updateFilters"></filtersearch> --}}

                   
                </div>
                <div class="box-body">
                	<div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
			              <li class="active"><a href="#tab_1" data-toggle="tab">Animales</a></li>
			              <li><a href="#tab_2" data-toggle="tab">Animales Vendidos</a></li>
			              {{-- <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li> --}}
			              
			              {{-- <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li> --}}
			            </ul>

			            <div class="tab-content">
			              <div class="tab-pane active" id="tab_1">
			              	@include('CMS::animales.porvender')
			              </div>
			              <!-- /.tab-pane -->
			              <div class="tab-pane" id="tab_2">
			                 @include('CMS::animales.vendidos')
			              </div>
			              
			            </div>
			            <!-- /.tab-content -->
			        </div>
                </div>


                
            </div>
           
        </div>
{{--          @include('CMS::citacuraes.create') --}}

        
    </section>

@endsection

@section('scripts')
    @include('CMS::proveedores.partials.scripts')
@stop