{{-- Mixins --}}
@include('CMS::partials.vueNotifications')

{{-- Components --}}
@include('inc.vuecomponents.vueModal')
@include('CMS::partials.vuePagination')
@include('inc.vuecomponents.vueSelect2_multi_tags')
@include('inc.vuecomponents.vueCalendar')
{{-- @include('inc.vuecomponents.vueSelect2') --}}
@include('CMS::partials.vueHelperFunctions')
@include('CMS::partials.vueFilterSearch')

<script>
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

    var app = new Vue ({

        mounted: function () {
        },

        el: '#app',

        mixins: [helperFunctions, notifications],

        data: {
            busqueda: '', //textbox de busqueda
            seleccion: 'fullName',


            tipos:[
                {text: 'Nombre completo', value:'fullName'},
                {text: 'Apellido Paterno', value:'Apellidopaterno'},
                {text: 'Apellido Materno', value:'Apellidomaterno'},
                {text: 'Especialidad', value:'nombre'},
                {text: 'Fecha', value:'fecha'},
                // {text: 'Fecha', value:'fecha'},
                ],
           	panelIndex:true,
           	// loading:true,
            pagination:null,
            dataJson:null,
            public_url: "{{ URL::to('/') }}/",
           

        },

        computed: {

        },
        methods:{

            getAnimales: function(url){

            },          


            //VUE HTTP RESOURCES
            save: function (data, route, action) {
                this.loading = true;
                var resource = this.$resource(route)
                resource.save(data).then(function (response) {                    
                    
                    // this.saveInAction = false;
                    if(action == "update") {
                        this.notification('fa fa-check-circle', 'OK!', "Cita actualizada correctamente.", 'topCenter');
                    } else {
                        this.notification('fa fa-check-circle', 'OK!', "Cita ingresada correctamente.", 'topCenter');
                    }
                    
                    this.regresar();
                    this.loading = false;
                    this.GetTuchmanCitas(this.dataRoute);
                }, function (response) {
                    this.loading = false;
                    this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
                    // this.errorsBag = response.data;
                })
            },

            delete: function (data, route) {
               
                var resource = this.$resource(route)
                resource.delete(data).then(function (response) {
                    this.notification('fa fa-check-circle', 'OK!', "Entrada borrada correctamente.", 'topCenter');
                    this.GetTuchmanCitas(this.dataRoute);
                }, function (response) {
                    this.notification('fa fa-exclamation-triangle', 'Error', "Se ha producido un error.", 'topCenter');
                })
            },
           
        }
    });

</script>