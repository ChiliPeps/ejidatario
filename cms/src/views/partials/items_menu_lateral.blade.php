{!! CMS::makeLinkForSidebarMenu('CMS::lotes.index', trans('CMS::lotes.lotes'), 'fa fa-archive') !!}
{!! CMS::makeLinkForSidebarMenu('CMS::proveedores.index', trans('CMS::proveedores.proveedores'), 'fa fa-institution ') !!}
{!! CMS::makeLinkForSidebarMenu('CMS::ventas.index', trans('CMS::ventas.ventas'), 'fa fa-dollar') !!}
{!! CMS::makeLinkForSidebarMenu('CMS::animales.index', trans('CMS::animales.animales'), 'fa fa-asterisk') !!}
{!! CMS::makeLinkForSidebarMenu('CMS::clientes.index', trans('CMS::clientes.clientes'), 'fa fa-male') !!}
{!! CMS::makeLinkForSidebarMenu('CMS::gastos.index', trans('CMS::gastos.gastos'), 'fa fa-file') !!}