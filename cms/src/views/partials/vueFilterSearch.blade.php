{{-- Requerimientos: Bootstrap 3, Vue.js 2, VueHelperFunctions.blade.php -> Lodash.js, Laravel 5.3 --}}

<script>
Vue.component('filtersearch', {
    props: ['selected', 'options'],
    template: `
    <div class="row">
        <div class="col-md-4">

            <div class="form-group" v-if="this.tipo !='fecha'">
                <label for="busqueda">Busqueda</label>
                <input label="Busqueda" name="busqueda" type="text" class="form-control" v-model="busqueda">
            </div>

            <div class="form-group" v-else>
                <label for="busqueda">Busqueda por Fechas</label>
                <input label="BusquedaF" name="busquedaF" type="text" class="form-control" v-model="busquedaF">
            </div>

        </div>
        <div class="col-md-4">

            <label for="tipo">Por</label>                        
            <select id="tipo" name="tipo" class="form-control" v-model="tipo" @change="filterSearch_tipo">
                <option v-for="o in options" :value="o.value">@{{ o.text }}</option>
            </select>

        </div>
    </div>
    `,
    mounted: function () {
        this.tipo = this.selected;
    },
    data: function () {
        return {
            tipo: '',
            busqueda: '',
            busquedaF:'',
        }
    },
    watch: {
        busqueda: function () {
            this.filterSearch();
        }
    },
    methods: {

            filterSearch: _.debounce(function () {
                this.$emit("updatefilters", { busqueda: this.busqueda, tipo: this.tipo, busquedaF: this.busquedaF});
            }, 500),

            filterSearch_tipo: _.debounce(function () {
                if(this.busqueda == '') { return; }
                this.$emit("updatefilters", { busqueda: this.busqueda, tipo: this.tipo, busquedaF: this.busquedaF });
            }, 500)
    }
});
</script>