<?php

return [
    'animales' => 'Animales',
    'create_new_animal' => 'Crear un articulo nuevo',
    'content'   => 'Contenido',
    'seo'       => 'SEO',
    'gallery'   => 'Galer�a',
    'publish'   => 'Publicar',
    'primary_image' => 'Imagen primaria',
    'msg_animal_created' => 'Articulo creado',
    'edit_animal' => 'Editar Articulo',
    'msg_animal_updated' => 'Articulo actualizado!',
    'unpublish' => 'No publicado',
    'msg_animal_published' => 'Articulo publicado!',
    'msg_animal_unpublished' => 'Articulo no publicado!',
    'msg_animal_deleted' => 'Articulo borrado!',
    'test_animales'=>'El modulo animales funciona',
];