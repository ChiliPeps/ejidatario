<?php

return [
    'clientes' => 'Clientes',
    'create_new_cliente' => 'Crear un articulo nuevo',
    'content'   => 'Contenido',
    'seo'       => 'SEO',
    'gallery'   => 'Galer�a',
    'publish'   => 'Publicar',
    'primary_image' => 'Imagen primaria',
    'msg_cliente_created' => 'Articulo creado',
    'edit_cliente' => 'Editar Articulo',
    'msg_cliente_updated' => 'Articulo actualizado!',
    'unpublish' => 'No publicado',
    'msg_cliente_published' => 'Articulo publicado!',
    'msg_cliente_unpublished' => 'Articulo no publicado!',
    'msg_cliente_deleted' => 'Articulo borrado!',
    'test_clientes'=>'El modulo clientes funciona',
];