<?php

return [
    'gastos' => 'Gastos',
    'create_new_gasto' => 'Crear un articulo nuevo',
    'content'   => 'Contenido',
    'seo'       => 'SEO',
    'gallery'   => 'Galer�a',
    'publish'   => 'Publicar',
    'primary_image' => 'Imagen primaria',
    'msg_gasto_created' => 'Articulo creado',
    'edit_gasto' => 'Editar Articulo',
    'msg_gasto_updated' => 'Articulo actualizado!',
    'unpublish' => 'No publicado',
    'msg_gasto_published' => 'Articulo publicado!',
    'msg_gasto_unpublished' => 'Articulo no publicado!',
    'msg_gasto_deleted' => 'Articulo borrado!',
    'test_gastos'=>'El modulo gastos funciona',
];