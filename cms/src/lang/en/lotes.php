<?php

return [
    'lotes' => 'Lotes',
    'create_new_lote' => 'Crear un articulo nuevo',
    'content'   => 'Contenido',
    'seo'       => 'SEO',
    'gallery'   => 'Galer�a',
    'publish'   => 'Publicar',
    'primary_image' => 'Imagen primaria',
    'msg_lote_created' => 'Articulo creado',
    'edit_lote' => 'Editar Articulo',
    'msg_lote_updated' => 'Articulo actualizado!',
    'unpublish' => 'No publicado',
    'msg_lote_published' => 'Articulo publicado!',
    'msg_lote_unpublished' => 'Articulo no publicado!',
    'msg_lote_deleted' => 'Articulo borrado!',
    'test_lotes'=>'El modulo lotes funciona',
];