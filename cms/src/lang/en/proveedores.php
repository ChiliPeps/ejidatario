<?php

return [
    'proveedores' => 'Proveedores',
    'create_new_proveedor' => 'Crear un articulo nuevo',
    'content'   => 'Contenido',
    'seo'       => 'SEO',
    'gallery'   => 'Galer�a',
    'publish'   => 'Publicar',
    'primary_image' => 'Imagen primaria',
    'msg_proveedor_created' => 'Articulo creado',
    'edit_proveedor' => 'Editar Articulo',
    'msg_proveedor_updated' => 'Articulo actualizado!',
    'unpublish' => 'No publicado',
    'msg_proveedor_published' => 'Articulo publicado!',
    'msg_proveedor_unpublished' => 'Articulo no publicado!',
    'msg_proveedor_deleted' => 'Articulo borrado!',
    'test_proveedores'=>'El modulo proveedores funciona',
];