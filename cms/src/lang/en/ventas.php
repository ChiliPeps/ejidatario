<?php

return [
    'ventas' => 'Ventas',
    'create_new_venta' => 'Crear un articulo nuevo',
    'content'   => 'Contenido',
    'seo'       => 'SEO',
    'gallery'   => 'Galer�a',
    'publish'   => 'Publicar',
    'primary_image' => 'Imagen primaria',
    'msg_venta_created' => 'Articulo creado',
    'edit_venta' => 'Editar Articulo',
    'msg_venta_updated' => 'Articulo actualizado!',
    'unpublish' => 'No publicado',
    'msg_venta_published' => 'Articulo publicado!',
    'msg_venta_unpublished' => 'Articulo no publicado!',
    'msg_venta_deleted' => 'Articulo borrado!',
    'test_ventas'=>'El modulo ventas funciona',
];