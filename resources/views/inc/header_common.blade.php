<!-- CSS Resources -->
{!! Html::style('css/bulma.min.css') !!}
{!! Html::style('css/font-awesome.min.css') !!}
{!! Html::style('css/base_LogReg.css') !!}

{{-- Programadores CSS --}}
{!! Html::style('css/dashboard.css') !!}

{{-- Stack for CSS --}}
@stack('css')
