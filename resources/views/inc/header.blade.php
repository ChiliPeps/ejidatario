@if(isset($user))
<nav class="nav has-shadow">
    <div class="container">
        <div class="nav-left">
            {!! Html::image("img/logo_min.png", "logo", ['class' => 'image']) !!}
        </div>

        <div class="nav-right nav-menu">
            <span class="nav-item">
                <a class="button" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <span class="icon"><i class="fa fa-sign-out"></i></span>
                    <span>Cerrar Sesión</span>
                </a>
            </span>
        </div>

        {{-- Hidden Logout Button --}}
        <div class="has-text-centered is-hidden-tablet" style="padding-top: 10px; padding-right: 10px;">
            <a class="button" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <span class="icon"><i class="fa fa-sign-out"></i></span>
                <span>Cerrar Sesión</span>
            </a>
        </div>

        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    </div>
</nav>
@endif