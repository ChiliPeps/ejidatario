@if(isset($user))
<footer class="footer">
  <div class="container">

  {{-- Call Button --}}
  <div class="block has-text-centered is-hidden-tablet">
    <a href="tel:6121835475" class="button is-info is-medium">
      <i class="fa fa-phone" aria-hidden="true" style="padding-top: 3px;margin-right: 5px;"></i> Llamar
    </a>
  </div>
  
    <div class="content has-text-centered">

      <div class="columns">
        <div class="column">
          <p class="title is-3"><strong>Teléfonos:</strong></p>
          <p class="subtitle is-5">cel: <a href="tel:6121835475">612 183-54-75</a>; oficina: <a href="tel:6121250458">612 125-04-58</a>.</p>
        </div>
        <div class="column">
          <p class="title is-3"><strong>Dirección:</strong></p>
          <p class="subtitle is-5">Carlos M Ezquerro #1600 col. Centro La Paz, Baja California Sur CP. 23000.</p>
        </div>
        <div class="column">
          <p class="title is-3"><strong>Correo:</strong></p>
          <p class="subtitle is-5"><a href="mailto:tuchmann@msn.com?subject=" target="_self" data-content="tuchmann@msn.com" data-type="mail">tuchmann@msn.com</a></p>
        </div>
      </div>

      <p>Aplicación Web del sitio <a target="_blank" href="http://www.curaeortodoncia.com">curaeortodoncia.com</a>.</p>

      <p>
        <a target="_blank" href="http://www.curaeortodoncia.com">
          {!! Html::image("img/logo_min.png", "logo", ['class' => '']) !!}
        </a>
      </p>

    </div>
  </div>
</footer>
@endif