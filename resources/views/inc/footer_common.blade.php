<!-- Jquery -->
{!! Html::script('js/jquery-2.1.4.min.js') !!}
<!-- Vue.js & Vue-Resource -->
{!! Html::script('js/vue/vue.js') !!}
{!! Html::script('js/vue/vue-resource.min.js') !!}

{{-- Stack Scripts --}}
@stack('scripts')

